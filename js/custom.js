$(document).ready(function ($){

    /* slick slider */
    $('.slider__wrapper').slick({
        autoplay: true,
        autoplaySpeed: 3000,
        arrows: true,
        infinite: true,
        speed: 2000,
        fade: true,
        cssEase: 'linear'
    });


});